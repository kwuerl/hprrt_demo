#version 430 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in VS_OUT {
      vec3 FragPos;
      vec3 Normal;
      vec3 Color;
      vec2 TexCoords;
   } vs_in[];


out VS_OUT {
       vec3 FragPos;
       vec3 Normal;
       vec3 Color;
       vec2 TexCoords;
       flat vec2 manualcoords[3];
       flat vec2 bounds_max;
       flat vec2 bounds_min;
} vs_out;


void main()
{
    vec2 bounds_max = vec2(0.0, 0.0);
    vec2 bounds_min = vec2(1.0, 1.0);
    for(int j = 0; j < 3 && j < gl_in.length(); j++)
    {
        bounds_max.x = max(bounds_max.x, vs_in[j].TexCoords.x);
        bounds_max.y = max(bounds_max.y, vs_in[j].TexCoords.y);
        bounds_min.x = min(bounds_min.x, vs_in[j].TexCoords.x);
        bounds_min.y = min(bounds_min.y, vs_in[j].TexCoords.y);
    }
    for (int i = 0; i < gl_in.length(); i++)
    {
        vs_out.Normal = vs_in[i].Normal;
        vs_out.FragPos = vs_in[i].FragPos;
        vs_out.TexCoords = vs_in[i].TexCoords;
        vs_out.Color = vs_in[i].Color;
        vs_out.manualcoords[i] = vs_in[i].TexCoords;
        vs_out.bounds_max = bounds_max;
        vs_out.bounds_min = bounds_min;
        gl_Position = gl_in[i].gl_Position;

        EmitVertex();
    }

    EndPrimitive();
}