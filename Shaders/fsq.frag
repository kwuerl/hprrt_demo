#version 430 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} fs_in;

uniform sampler2D tex;

void main()
{
    FragColor = texture2D(tex, fs_in.TexCoords);
}