#version 430 core

layout (location = 0) out vec4 FragPos;
layout (location = 1) out vec4 Normal;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} fs_in;

uniform sampler2D tex;

void main()
{
    FragPos = vec4(fs_in.FragPos, 1.0);
    Normal = vec4(fs_in.Normal, 1.0);
}