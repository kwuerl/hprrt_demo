#version 430 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec3 aColor;
layout (location = 3) in vec2 aTexCoords;

// declare an interface block; see 'Advanced GLSL' for what these are.
out VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} vs_out;

void main()
{
    vs_out.Normal = aNormal;
    vs_out.FragPos = aPos;
    vs_out.TexCoords = aTexCoords;
    gl_Position = vec4(aTexCoords.xy*2.0 - vec2(1.0,1.0) ,0.0, 1.0);
}