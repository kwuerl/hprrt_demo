

#version 430 core

out vec4 FragColor;

in VS_OUT {
       vec3 FragPos;
       vec3 Normal;
       vec3 Color;
       vec2 TexCoords;
       flat vec2 manualcoords[3];
       flat vec2 bounds_max;
       flat vec2 bounds_min;
} fs_in;

uniform vec3 inColor;
uniform vec3 lightPos;
uniform vec3 viewPos;
bool blinn = false;

uniform sampler2D tex;
uniform sampler2D texCol;
uniform bool hasCol;

//uniform float u_pix_count;
//uniform float u_pix_count_inv;
uniform float u_pix;
//uniform float u_pix_half;

const int winborder = 1;
const int winsz = winborder*2 + 1;
const int winszp = winsz*winsz;
const int winszphalf = winszp/2 + 1;

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
    vec3 color = inColor;
    // ambient
    vec3 ambient = 0.05 * color;
    // diffuse
    vec3 lightDir = normalize(lightPos - fs_in.FragPos);
    vec3 normal = normalize(fs_in.Normal);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * color;
    // specular
    vec3 viewDir = normalize(viewPos - fs_in.FragPos);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = 0.0;
    if(blinn)
    {
        vec3 halfwayDir = normalize(lightDir + viewDir);
        spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
    }
    else
    {
        vec3 reflectDir = reflect(-lightDir, normal);
        spec = pow(max(dot(viewDir, reflectDir), 0.0), 8.0);
    }
    vec3 specular = vec3(0.3) * spec; // assuming bright white light color
    vec4 totalc = vec4(ambient + diffuse + specular, 1.0);

    vec3 window[winszp];
    //vec2 pixuv = ceil(fs_in.TexCoords*u_pix_count)*u_pix_count_inv - vec2(u_pix_half,u_pix_half);
    int s = 0;
    for(int i = 0; i < winsz; i++)
    {
        for(int j = 0; j < winsz; j++)
        {
            vec2 uv = fs_in.TexCoords + vec2(u_pix*(i-winborder-1),u_pix*(j-winborder-1));
            vec2 uv1 = max(uv, fs_in.bounds_min);
            vec2 uv2 = min(uv1, fs_in.bounds_max);
            window[s++] = vec3(texture2D(tex, uv2));
        }
    }

    for (int j = 0; j < winszphalf; ++j)
    {
        //   Find position of minimum element
        ivec3 min = ivec3(j);
        for (int l = j + 1; l < winszp; ++l)
        {
            if (window[l].x < window[min.x].x)
                min.x = l;
            if (window[l].y < window[min.y].y)
                min.y = l;
            if (window[l].z < window[min.z].z)
                min.z = l;
        }
        //   Put found minimum element in its place
        const vec3 temp = window[j];
        window[j].x = window[min.x].x;
        window[j].y = window[min.y].y;
        window[j].z = window[min.z].z;
        window[min.x].x = temp.x;
        window[min.y].y = temp.x;
        window[min.z].z = temp.x;
    }

    vec4 img = vec4(inColor,1.0);
    //img = vec4(1,1,1,1);
    if(hasCol)
    {
        img = texture2D(texCol, fs_in.TexCoords);
        vec3 total = window[winszphalf-1].xxx*img.xyz;
        FragColor = vec4(total,img.w);
    }
    else
    {
        FragColor = vec4(window[winszphalf-1].xxx, 1.0)*vec4(fs_in.Color,1.0)*img;
    }
    //vec4 t = texture2D(tex, fs_in.TexCoords-vec2(u_pix, u_pix));
    //FragColor = vec4(t.xxx, 1.0) *vec4(fs_in.Color,1.0)*img;
    //FragColor = mix(totalc, vec4(window[winszphalf-1].xxx, 1.0), 1)*vec4(fs_in.Color,1.0)*img;

}