#version 430 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in VS_OUT {
      vec3 FragPos;
      vec3 Normal;
      vec2 TexCoords;
   } vs_in[];


out VS_OUT {
       vec3 FragPos;
       vec3 Normal;
       vec2 TexCoords;
    } vs_out;

uniform float u_half_pix;

void main()
{
    vec4 central = gl_in[0].gl_Position + gl_in[1].gl_Position + gl_in[2].gl_Position;
    central *= 0.3333333;

    for (int i = 0; i < gl_in.length(); i++)
    {
        vs_out.Normal = vs_in[i].Normal;
        vs_out.FragPos = vs_in[i].FragPos;
        vs_out.TexCoords = vs_in[i].TexCoords;
        gl_Position = gl_in[i].gl_Position;

        if(gl_Position.x > central.x) gl_Position.x += u_half_pix;
        else gl_Position.x -= 0*u_half_pix;

        if(gl_Position.y > central.y) gl_Position.y += u_half_pix;
        else gl_Position.y -= 0*u_half_pix;



        EmitVertex();
    }

    EndPrimitive();
}