/**********************************************************************
 Copyright (c) 2016 Advanced Micro Devices, Inc. All rights reserved.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ********************************************************************/
#ifndef KERNEL_CL
#define KERNEL_CL

#include "tinymt32_jump.clh"

#define EPSILON 0.001f

typedef struct _Ray
{
    /// xyz - origin, w - max range
    float4 o;
    /// xyz - direction, w - time
    float4 d;
    /// x - ray mask, y - activity flag
    int2 extra;
    /// Padding
    float2 padding;
} Ray;

typedef struct _Intersection
{
    // id of a shape
    int shapeid;
    // Primitive index
    int primid;
    // Padding elements
    int padding0;
    int padding1;

    // uv - hit barycentrics, w - ray distance
    float4 uvwt;
} Intersection;

const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;


__kernel void GenerateShadowRays(__global Ray* rays,
                                    read_only image2d_t posData,
                                    read_only image2d_t normData,
                                    int width,
                                    int height,
                                    float4 lightPos,
                                    uint2 randoms)
{
    int2 globalid;
    globalid.x  = get_global_id(0);
    globalid.y  = get_global_id(1);


    // Check borders
    if (globalid.x < width && globalid.y < height)
    {
        float4 pos = read_imagef(posData, sampler, globalid);
        float4 norm = read_imagef(normData, sampler, globalid);
        int k = globalid.y * width + globalid.x;
        int l = width*height;

        tinymt32j_t tinymt;
        tinymt32j_init_jump(&tinymt, randoms.x/l + k);

        float rnd1 = tinymt32j_single01(&tinymt);
        float rnd2 = tinymt32j_single01(&tinymt);

        float jitter = 10.0;

        float3 addit = norm.xyz * 0.01f;
        rays[k].o.xyz = pos.xyz + addit;
        rays[k].d.x = lightPos.x + rnd1*jitter*2.0 - jitter;
        rays[k].d.y = lightPos.y  + rnd2*jitter*2.0 - jitter;
        rays[k].d.z = lightPos.z;
        rays[k].o.w = 200.0;

        float dotp = dot(rays[k].d.xyz, norm.xyz);

        if(pos.w == 0.0 || dotp <= 0.0)
            rays[k].extra.x = 0;
        else
            rays[k].extra.x = 0xFFFFFFFF;
        rays[k].extra.y = 0xFFFFFFFF;
    }
}


__kernel void GenerateAORays(__global Ray* rays,
                                    read_only image2d_t posData,
                                    read_only image2d_t normData,
                                    int width,
                                    int height,
                                    float4 lightPos,
                                    uint2 randoms)
{
    int2 globalid;
    globalid.x  = get_global_id(0);
    globalid.y  = get_global_id(1);


    // Check borders
    if (globalid.x < width && globalid.y < height)
    {
        float4 pos = read_imagef(posData, sampler, globalid);
        float4 norm = read_imagef(normData, sampler, globalid);
        int k = globalid.y * width + globalid.x;
        int l = width*height;

        tinymt32j_t tinymt;
        tinymt32j_init_jump(&tinymt, randoms.x / l + k);
        float rnd1 = tinymt32j_single01(&tinymt);
        float rnd2 = tinymt32j_single01(&tinymt)*2.0 -1.0;
        float rnd3 = tinymt32j_single01(&tinymt)*2.0 -1.0;


        float3 helpv;
        helpv.x = norm.z;
        helpv.y = norm.x*2;
        helpv.z = norm.y*5;

        float3 bx = normalize(norm.xyz);
        float3 by = normalize(cross(normalize(helpv), bx));
        float3 bz = normalize(cross(by, bx));

        float3 addit = norm.xyz * 0.001f;
        rays[k].o.xyz = pos.xyz + addit;
        rays[k].d.xyz = normalize(bx*rnd1 + by*rnd2 + bz*rnd3);
        rays[k].o.w = 0.8;

        if(pos.w == 0.0)
            rays[k].extra.x = 0;
        else
            rays[k].extra.x = 0xFFFFFFFF;
        rays[k].extra.y = 0xFFFFFFFF;
    }
}


__kernel void ShadingDirect(__global Ray* rays,
                                    //__global Intersection* isect,
                                    __global int* occl,
                                    read_only image2d_t posData,
                                    read_only image2d_t normData,
                                    read_only image2d_t lightDataIn,
                                    write_only image2d_t lightDataOut,
                                    int iteration,
                                    int width,
                                    int height)
{
    int2 globalid;
    globalid.x  = get_global_id(0);
    globalid.y  = get_global_id(1);


    // Check borders
    if (globalid.x < width && globalid.y < height)
    {
        float4 pos = read_imagef(posData, sampler, globalid);
        float4 norm = read_imagef(normData, sampler, globalid);
        float4 grayv = read_imagef(lightDataIn, sampler, globalid);
        float it = iteration;

        int k = globalid.y * width + globalid.x;

        float3 raysd = normalize(rays[k].d.xyz);

        //int shape_id = isect[k].shapeid;
        //int prim_id = isect[k].primid;

        if (/*shape_id != -1 && prim_id != -1 &&*/ occl[k] != -1)
        {
            grayv.x = (grayv.x * it + 0.0) / (it+1.0);
        }
        else
        {
            float dotp = dot(norm.xyz, raysd);
            float mx = max(dotp, 0.0f);
            grayv.x = (grayv.x * it + mx) / (it+1.0);
        }

        write_imagef(lightDataOut, globalid, grayv);
    }
}

__kernel void ShadingAO(__global Ray* rays,
                                    //__global Intersection* isect,
                                    __global int* occl,
                                    read_only image2d_t posData,
                                    read_only image2d_t normData,
                                    read_only image2d_t lightDataIn,
                                    write_only image2d_t lightDataOut,
                                    int iteration,
                                    int width,
                                    int height)
{
    int2 globalid;
    globalid.x  = get_global_id(0);
    globalid.y  = get_global_id(1);


    // Check borders
    if (globalid.x < width && globalid.y < height)
    {
        float4 pos = read_imagef(posData, sampler, globalid);
        float4 norm = read_imagef(normData, sampler, globalid);
        float4 grayv = read_imagef(lightDataIn, sampler, globalid);
        float it = iteration;

        int k = globalid.y * width + globalid.x;

        float3 raysd = normalize(rays[k].d.xyz);

        //int shape_id = isect[k].shapeid;
        //int prim_id = isect[k].primid;

        if (/*shape_id != -1 && prim_id != -1 &&*/ occl[k] != -1)
        {
            grayv.x = (grayv.x * it + 0.3) / (it+1.0);
        }
        else
        {
            float dotp = dot(norm.xyz, raysd);
            float mx = max(dotp, grayv.x);
            grayv.x = (grayv.x * it + 1) / (it+1.0);
        }

        write_imagef(lightDataOut, globalid, grayv);
    }
}
#endif //KERNEL_CL