//
// Created by ligx on 6/28/18.
//

#ifndef GLITTER_OBJ_ACTOR_H
#define GLITTER_OBJ_ACTOR_H


#include "scene_params.h"
#include "shader.hpp"
#include "vertex_buffer.h"
#include "actor.h"
#include "base_rt_actor.h"
#include "framebuffer.h"

class ObjActor : public BaseRTActor
{
private:
    struct ObjVertex {
        glm::vec3 pos;
        glm::vec3 norm;
        glm::vec3 col;
        glm::vec2 uv;
    };
public:
    ObjActor(int id, std::string path, glm::mat4 tf);
    ~ObjActor();

    RayMesh getMesh();
    void renderUVPosNormTextures(
            int width,
            int height,
            std::shared_ptr<Texture> texturePos,
            std::shared_ptr<Texture> textureNorm
    ) override;

    void render(SceneParams params) override;
    std::shared_ptr<Texture> textureColor;
    glm::vec3 color;
private:
    Shader objShader;
    Shader uvPosShader;
    VertexBuffer<ObjVertex, /* POS */ VB_FLOAT_3, /* NORM */ VB_FLOAT_3, /* COL */ VB_FLOAT_3, /* UV */ VB_FLOAT_2> buffer;
    std::vector<ObjVertex> vertexData;
    std::vector<int> idxData;

    FrameBuffer *fb;
};


#endif //GLITTER_OBJ_ACTOR_H
