//
// Created by ligx on 5/28/18.
//

#ifndef GLITTER_ACTOR_H
#define GLITTER_ACTOR_H

#include <glm/detail/type_mat.hpp>
#include "scene_params.h"
#include "texture.h"

#include <radeon_rays.h>

using namespace RadeonRays;

class Actor {
protected:
    int id;
public:
    Actor(int id) : id(id) {};
    ~Actor() {};

    virtual void render(SceneParams params) = 0;
};

#endif //GLITTER_ACTOR_H
