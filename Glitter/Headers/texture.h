//
// Created by ligx on 7/2/18.
//

#ifndef GLITTER_TEXTURE_H
#define GLITTER_TEXTURE_H

#include <vector>
#include <string>
#include <glad/glad.h>
#include <glm/vec4.hpp>

class Texture
{
public:
    Texture(int width, int height, GLenum iformat = GL_RGBA, GLenum type = GL_UNSIGNED_BYTE);
    ~Texture();
    void upload(const std::vector<unsigned char> &tex_data);
    void upload(const unsigned char *tex_data);
    void read(std::vector<glm::vec4> &tex_data);
    void bind();
    void unbind();
    GLuint getId() { return textureId; };

    int getHeight() { return height; };
    int getWidth() { return width; };

    void writeToFile(std::string filename);

private:
    GLuint textureId;
    int width;
    int height;
};


#endif //GLITTER_TEXTURE_H
