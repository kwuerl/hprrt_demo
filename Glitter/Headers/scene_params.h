//
// Created by ligx on 6/26/18.
//

#ifndef GLITTER_SCENEPARAMS_H
#define GLITTER_SCENEPARAMS_H


#include <glm/detail/type_mat.hpp>
#include <glm/vec3.hpp>
#include <glm/detail/type_mat4x4.hpp>

struct SceneParams
{
    glm::vec3 viewPos;
    glm::vec3 lightPos;
    glm::mat4 view;
    glm::mat4 projection;
};


#endif //GLITTER_SCENEPARAMS_H
