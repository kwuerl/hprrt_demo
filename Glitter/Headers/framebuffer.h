//
// Created by ligx on 7/6/18.
//

#ifndef GLITTER_FRAMEBUFFER_H
#define GLITTER_FRAMEBUFFER_H


#include <glad/glad.h>
#include <memory>
#include "texture.h"

class FrameBuffer
{
public:
    FrameBuffer(int width, int height);
    ~FrameBuffer();
    void addTexture(int pos, std::shared_ptr<Texture> texture);
    void bind();
    void unbind();
private:
    GLuint fbId;
    GLuint depthrenderbuffer;
};


#endif //GLITTER_FRAMEBUFFER_H
