//
// Created by ligx on 5/28/18.
//

#ifndef GLITTER_CUBE_ACTOR_H
#define GLITTER_CUBE_ACTOR_H

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include "actor.h"
#include "vertex_buffer.h"
#include "base_rt_actor.h"
#include <shader.hpp>


class CubeActor : public BaseRTActor {
private:
    struct CubeVertex {
        glm::vec3 pos;
        glm::vec3 norm;
        glm::vec2 uv;
    };
public:
    CubeActor(int id);
    ~CubeActor();

    RayMesh getMesh() override;
    void renderUVPosNormTextures(
            int width,
            int height,
            std::shared_ptr<Texture> texturePos,
            std::shared_ptr<Texture> textureNorm
    ) override {};

    void render(SceneParams params) override;
private:
    Shader cubeShader;
    std::vector<CubeVertex> vertexData;
    std::vector<int> idxData;
    VertexBuffer<CubeVertex, VB_FLOAT_3, VB_FLOAT_3, VB_FLOAT_2> buffer;
};

#endif //GLITTER_CUBE_ACTOR_H
