//
// Created by ligx on 7/17/18.
//

#ifndef GLITTER_RAYTRACER_H
#define GLITTER_RAYTRACER_H


#include <thread>
#include <vector>
#include <CLWContext.h>
#include <CLWKernel.h>
#include <CLWImage2D.h>
#include <CLWBuffer.h>
#include <CLWProgram.h>
#include <CL/cl_platform.h>
#include "texture.h"
#include <radeon_rays.h>
#include <GL/glx.h>
#include <mutex>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#define GLFW_EXPOSE_NATIVE_X11
#define GLFW_EXPOSE_NATIVE_GLX
#include <GLFW/glfw3native.h>
#include <condition_variable>
#include <stdint-gcc.h>


class SharedImage
{
private:
    enum LockMode
    {
        LOCK_GL,
        LOCK_CL,
        LOCK_NONE
    };

public:
    enum MemoryMode
    {
        MMODE_READ_ONLY,
        MMODE_WRITE_ONLY,
        MMODE_READ_WRITE,
        MMODE_NONE
    };
    SharedImage(int width, int height, GLenum iformat = GL_RGBA, GLenum type = GL_UNSIGNED_BYTE, MemoryMode mode = MMODE_READ_WRITE)
    {
        mtx = new std::mutex();
        lockMode = new LockMode(LOCK_NONE);
        memoryMode = MMODE_NONE;
        glTexture = new Texture(width, height, iformat, type);
        clImage = new CLWImage2D();
    };

public:
    ~SharedImage()
    {
        std::unique_lock<std::mutex> lock(*mtx);
        if(*lockMode != LOCK_CL) delete clImage;

        // The image is still locked so mark it for destruction
        if(*lockMode != LOCK_NONE)
        {
            *lockMode = LOCK_NONE;
        }
        else
        {
            delete lockMode;
            lock.unlock();
            delete mtx;
        }
    };

    // lock SharedImage for use with OpenGL.
    std::shared_ptr<Texture> lockGL()
    {
        std::unique_lock<std::mutex> lock(*mtx);

        if(*lockMode != LOCK_NONE) throw std::runtime_error("Cannot lock SharedImage for GL: Already used");

        *lockMode = LOCK_GL;


        return std::shared_ptr<Texture>(glTexture, [valL = lockMode, valM = mtx] (Texture* texture) {
            std::unique_lock<std::mutex> lock(*valM);

            // The SharedImage object was already destroyed
            if(*valL == LOCK_NONE)
            {
                delete texture;
                delete valL;
                lock.unlock();
                delete valM;
            }
            else
            {
                *valL = LOCK_NONE;
            }
        });
    };

    // lock SharedImage for use with OpenCL.
    std::shared_ptr<CLWImage2D> lockCL(CLWContext ctx, MemoryMode mode)
    {
        std::unique_lock<std::mutex> lock(*mtx);

        if (*lockMode != LOCK_NONE)
        {
            throw std::runtime_error("Cannot lock SharedImage for CL: Already used");
        }

        *lockMode = LOCK_CL;


        if(mode == MMODE_NONE)
        {
            return nullptr;
        }
        if(memoryMode != mode || lastCtx != ctx)
        {
            cl_mem_flags flags;
            switch (mode)
            {
                case MMODE_READ_ONLY:
                    flags = CL_MEM_READ_ONLY;
                    break;
                case MMODE_WRITE_ONLY:
                    flags = CL_MEM_WRITE_ONLY;
                    break;
                case MMODE_READ_WRITE:
                    flags = CL_MEM_READ_WRITE;
                    break;
            }
            *clImage = CLWImage2D::CreateFromGLTexture(ctx, flags, glTexture->getId());
            memoryMode = mode;
            lastCtx = ctx;
        }

        ctx.AcquireGLObjects(0, {*clImage});

        return std::shared_ptr<CLWImage2D>(clImage, [valL = lockMode, valM = mtx, valC = lastCtx, valT = glTexture] (CLWImage2D *image) {
            std::unique_lock<std::mutex> lock(*valM);

            valC.ReleaseGLObjects(0, {*image});

            // The SharedImage object was already destroyed
            if (*valL == LOCK_NONE)
            {
                delete image;
                delete valT;
                delete valL;
                lock.unlock();
                delete valM;
            }
            else
            {
                *valL = LOCK_NONE;
            }
        });
    };

private:
    CLWContext lastCtx;
    LockMode* lockMode;
    MemoryMode memoryMode;
    std::mutex* mtx;
    Texture* glTexture;
    CLWImage2D* clImage;
};

struct RayMesh
{
    const float* vertices;
    const size_t szVertices;

    const int *indices;
    const size_t szIndices;

    const size_t byteStride;
};

class Raytracable
{
public:
    Raytracable(int id) : id(id) {};
    virtual void initRT(int width, int height, uint8_t fill = 0) = 0;
    virtual RayMesh getMesh() = 0;
    //virtual std::shared_ptr<SharedImage> getPosTexture() = 0;
    //virtual std::shared_ptr<SharedImage> getNormTexture() = 0;
    virtual std::shared_ptr<SharedImage> getLastLightTexture() = 0;
    virtual void swapLightTexture(std::shared_ptr<SharedImage>& newImage) = 0;
    virtual void renderUVPosNormTextures(
            int width,
            int height,
            std::shared_ptr<Texture> texturePos,
            std::shared_ptr<Texture> textureNorm
    ) = 0;
    int getID() { return id; };
protected:
    int id;
};


class Raytracer
{
public:
    Raytracer(int maxwidth, int maxheight, GLFWwindow* mWindowParent);
    ~Raytracer();
    void start();
    void tickMainThread();

    void addTracable(std::shared_ptr<Raytracable> tracable, bool source = true, bool target = true);


private:
    enum RayType
    {
        RT_AMBIENT,
        RT_DIRECT
    };

    struct TraceableItem
    {
        std::shared_ptr<Raytracable> tracable;
        double calcTime;
        int iteration;
    };
    /* init */
    GLFWwindow* mWindow;
    //GLFWwindow* mWindow2;
    std::shared_ptr<RadeonRays::IntersectionApi> api;
    CLWContext ctx;
    CLWProgram clProgram;
    glm::ivec2 rayTexMaxSize;
    void initCL(const GLXContext &glcCtx, const Display* x11Display);
    void initRR();

    /* threading */
    std::thread runthread;
    bool stopThread;
    void run();

    /* container */
    std::vector<TraceableItem> tracables;
    std::vector<RadeonRays::Shape*> shapes;
    std::mutex mtxTracables;
    int shapeIDNext;

    /* tracing */
    void createRaysCL(std::shared_ptr<Raytracable> tracable, RayType type, int iteration);
    void calcIllumCL(std::shared_ptr<Raytracable> tracable, RayType type, int iteration);

    std::shared_ptr<SharedImage> textureNorm;
    std::shared_ptr<SharedImage> texturePos;
    std::shared_ptr<Raytracable> currTaceable;
    int currTaceableIt;
    bool renderedPosNorm;
    std::condition_variable cv;
    std::mutex renderMtx;

    std::shared_ptr<SharedImage> targetTex;

    CLWBuffer<RadeonRays::ray> rayBuf;
    CLWBuffer<int> occlBuf;

    std::unique_ptr<RadeonRays::Buffer> rayBuffer;
    std::unique_ptr<RadeonRays::Buffer> occlBuffer;
};


#endif //GLITTER_RAYTRACER_H
