//
// Created by ligx on 5/28/18.
//

#ifndef GLITTER_VERTEX_BUFFER_H
#define GLITTER_VERTEX_BUFFER_H

#include <glad/glad.h>
#include <vector>
#include <array>

#define CHECK_GL_ERROR_VB checkOpenGLError(__FILE__, __LINE__)

#define LAYOUT_OPT(name_v, size_v, type_v, ntype_v) \
struct name_v { \
    static size_t sizeBytes() { return sizeof(ntype_v); }; \
    static size_t size() { return size_v; }; \
    static GLenum type() { return type_v; }; \
};

LAYOUT_OPT(VB_FLOAT,   1, GL_FLOAT, GLfloat);
LAYOUT_OPT(VB_FLOAT_2, 2, GL_FLOAT, GLfloat);
LAYOUT_OPT(VB_FLOAT_3, 3, GL_FLOAT, GLfloat);
LAYOUT_OPT(VB_FLOAT_4, 4, GL_FLOAT, GLfloat);


struct ParamReflector {
public:
    virtual bool isLast()=0;
    virtual size_t sizeBytes()=0;
    virtual size_t size()=0;
    virtual GLenum type()=0;
    virtual ParamReflector* next()=0;
};
template<typename T, typename... TT>
struct ParamReflectorT : public ParamReflector {
    ParamReflectorT<TT...> nextV;
    bool isLast() override {return false;};
    size_t sizeBytes() override {return T::sizeBytes();};
    size_t size() override {return T::size();};
    GLenum type() override {return T::type();};
    ParamReflector* next() override { return &nextV;};
};
template<typename T>
struct ParamReflectorT<T> : public ParamReflector {
    bool isLast() override {return false;};
    size_t sizeBytes() override {return T::sizeBytes();};
    size_t size() override {return T::size();};
    GLenum type() override {return T::type();};
    ParamReflector* next() override {return NULL;};
};

//const int S, const std::array<size_t, S> & layout_l, const std::array<GLenum, S> & types_l
template <typename T, typename ... Layout>
class VertexBuffer {
private:


public:
    VertexBuffer()
    {
        ParamReflectorT<Layout...> reflect;

        ParamReflector* curr = &reflect;
        int i = 0;
        total = 0;
        while(curr != NULL)
        {
            layout[i] = curr->size();
            layoutB[i] = curr->sizeBytes();
            types[i] = curr->type();
            total += layout[i];
            curr = curr->next();
            i++;
        }
        glGenVertexArrays(1, &vaoId); CHECK_GL_ERROR_VB;
        glGenBuffers(1, &vboId); CHECK_GL_ERROR_VB;


        /*glBindVertexArray(vaoId); CHECK_GL_ERROR_VB;
        glBindBuffer(GL_ARRAY_BUFFER, vboId); CHECK_GL_ERROR_VB;
        glBindVertexArray(0); CHECK_GL_ERROR_VB;*/
    };
    ~VertexBuffer()
    {
        glDeleteBuffers(1, &vboId); CHECK_GL_ERROR_VB;
    }

    void bind()
    {
        glBindVertexArray(vaoId); CHECK_GL_ERROR_VB;
    }

    void unbind()
    {
        glBindVertexArray(0); CHECK_GL_ERROR_VB;
    }

    size_t size()
    {
        return size_v;
    }

    void attach()
    {
        std::array<int, sizeof...(Layout)> attributes;
        for(int i = 0; i < sizeof...(Layout); i++)
        {
            attributes[i] = i;
        }
        attachToVertexAttributes(attributes);
    }

    void attachToVertexAttributes(const std::array<int, sizeof...(Layout)> attributes)
    {
        this->bind();
        glBindBuffer(GL_ARRAY_BUFFER, vboId); CHECK_GL_ERROR_VB;
        int offs = 0;
        for(int i = 0; i < sizeof...(Layout); i++)
        {
            printf("--- %i %i %i %i %i \n", attributes[i], layout[i], types[i], sizeof(T), offs);
            glEnableVertexAttribArray(i);
            glVertexAttribPointer(attributes[i],
                                  layout[i],  // size
                                  types[i],   // type
                                  GL_FALSE,   // normalized
                                  sizeof(T),//total,      // stride
                                  (void*)offs); CHECK_GL_ERROR_VB;
            offs += layoutB[i]*layout[i];
        }
        this->unbind();
        glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR_VB;

    }

    void upload(std::vector<T> upload)
    {
        glBindBuffer(GL_ARRAY_BUFFER, vboId); CHECK_GL_ERROR_VB;
        printf("sizeof %i \n", sizeof(T));
        glBufferData(GL_ARRAY_BUFFER, sizeof(T)*upload.size(), upload.data(), GL_STATIC_DRAW); CHECK_GL_ERROR_VB;
        size_v = upload.size();
        //glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR_VB;

    }

    void checkOpenGLError(const char* file, int line)
    {
        GLenum error = glGetError();
        if (error != GL_NO_ERROR)
        {
            printf("OPENGL ERROR # %i (line %i in %s) \n", error, line, file);
        }
    }

private:
    GLuint vboId;
    GLuint vaoId;
    int total;
    int size_v;
    std::array<int, sizeof...(Layout)> layout;
    std::array<int, sizeof...(Layout)> layoutB;
    std::array<GLenum, sizeof...(Layout)> types;
};

#endif //GLITTER_VERTEX_BUFFER_H
