//
// Created by ligx on 30.06.18.
//

#ifndef GLITTER_FSQUAD_ACTOR_H
#define GLITTER_FSQUAD_ACTOR_H

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include "actor.h"
#include "vertex_buffer.h"
#include <shader.hpp>

class FSQuadActor : public Actor {
private:
    struct QuadVertex {
        glm::vec3 pos;
        glm::vec3 norm;
        glm::vec2 uv;
    };
public:
    FSQuadActor(int id);
    ~FSQuadActor();

    void render(SceneParams params) override;
    void setTexture(std::shared_ptr<Texture> tex) { this->tex = tex; };
private:
    Shader quadShader;
    std::vector<QuadVertex> vertexData;
    VertexBuffer<QuadVertex, VB_FLOAT_3, VB_FLOAT_3, VB_FLOAT_2> buffer;
    std::shared_ptr<Texture> tex;
};


#endif //GLITTER_FSQUAD_ACTOR_H
