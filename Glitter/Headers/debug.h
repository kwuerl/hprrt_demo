//
// Created by ligx on 6/18/18.
//

#ifndef GLITTER_DEBUG_H
#define GLITTER_DEBUG_H

#include <glad/glad.h>

void checkOpenGLError(const char* file, int line);
#define CHECK_GL_ERROR checkOpenGLError(__FILE__, __LINE__)

#endif //GLITTER_DEBUG_H
