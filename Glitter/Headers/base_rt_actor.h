//
// Created by ligx on 7/8/18.
//

#ifndef GLITTER_BASE_RT_ACTOR_H
#define GLITTER_BASE_RT_ACTOR_H


#include <CLWImage2D.h>
#include <CLWBuffer.h>
#include <CLWProgram.h>
#include <cstdint>
#include <stdint-gcc.h>
#include "actor.h"
#include "raytracer.h"

class BaseRTActor : public Raytracable, public Actor
{
public:
    BaseRTActor(int id);
    ~BaseRTActor();

    virtual void render(SceneParams params) = 0;
    virtual RayMesh getMesh() = 0;

    void initRT(int width, int height, uint8_t fill = 0) override;

    //std::shared_ptr<SharedImage> getPosTexture() override;
    //std::shared_ptr<SharedImage> getNormTexture() override;
    std::shared_ptr<SharedImage> getLastLightTexture() override;
    void swapLightTexture(std::shared_ptr<SharedImage>& newImage) override;

    //std::shared_ptr<SharedImage> textureNorm;
    //std::shared_ptr<SharedImage> texturePos;
    std::shared_ptr<SharedImage> textureLightLast;
    std::shared_ptr<SharedImage> textureLightCurr;
protected:
    virtual void renderUVPosNormTextures(
            int width,
            int height,
            std::shared_ptr<Texture> texturePos,
            std::shared_ptr<Texture> textureNorm
    ) = 0;
    void swapReal();
    bool shouldSwap;
    std::mutex mtx;
};


#endif //GLITTER_BASE_RT_ACTOR_H
