//
// Created by ligx on 7/6/18.
//

#include "framebuffer.h"

FrameBuffer::FrameBuffer(int width, int height) {
    glGenFramebuffers(1, &fbId);

    glBindFramebuffer(GL_FRAMEBUFFER, fbId);

    glGenRenderbuffers(1, &depthrenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

}

FrameBuffer::~FrameBuffer() {
    glDeleteFramebuffers(1, &fbId);
    glDeleteRenderbuffers(1, &depthrenderbuffer);
}

void FrameBuffer::addTexture(int pos, std::shared_ptr<Texture>texture) {
    this->bind();
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + pos, texture->getId(), 0);
    this->unbind();
}

void FrameBuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, fbId);
}

void FrameBuffer::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

