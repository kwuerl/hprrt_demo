//
// Created by ligx on 6/28/18.
//

#include <cstdio>
#include "debug.h"


void checkOpenGLError(const char* file, int line)
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        printf("OPENGL ERROR # %i (line %i in %s)\n", error, line, file);
    }
}