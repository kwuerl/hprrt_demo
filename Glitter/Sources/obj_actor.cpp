//
// Created by ligx on 6/28/18.
//

#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include "obj_actor.h"
#import "debug.h"
#import <glm/gtc/matrix_inverse.hpp>
#include <framebuffer.h>
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/string_cast.hpp"

ObjActor::ObjActor(int id, std::string path, glm::mat4 tf) : BaseRTActor(id) {
    glm::mat3 normalmat = glm::inverseTranspose(glm::mat3(tf));
    const struct aiScene* scene;
    scene = aiImportFile(path.c_str(),aiProcessPreset_TargetRealtime_MaxQuality);
    if (!scene) throw "Could not load scene";

    //glm::vec2 min_uv = {1.0,1.0};
    //glm::vec2 max_uv = {0.0,0.0};

    for(int m_index = 0; m_index < scene->mNumMeshes; m_index++)
    {
        aiMesh* mesh = scene->mMeshes[m_index];
        for(int f_index = 0; f_index < mesh->mNumFaces; f_index++)
        {
            aiFace* face = &mesh->mFaces[f_index];
            std::vector<int> l;
            bool recalc_normals = true;

            if (face->mNumIndices==3)
                l = {0,1,2};
            else
                l = {0,1,2,0,2,3};

            for(const int& i : l)
            {
                int idx = face->mIndices[i];
                if(idx > mesh->mNumVertices) break;
                aiVector3D v = mesh->mVertices[idx];
                aiVector3D n = mesh->mNormals[idx];
                aiVector3D u = mesh->mTextureCoords[0][idx];
                aiColor4D c;
                if(mesh->HasVertexColors(0))
                    c = mesh->mColors[0][idx];
                else
                    c = {1,1,1,1};
                glm::vec4 gv = {v.x,v.y,v.z, 1};
                glm::vec3 gn = {n.x, n.y, n.z};
                glm::vec2 gu = {u.x, u.y};
                glm::vec3 gc = {c.r, c.g, c.b};
                //std::cout<<glm::to_string(normalmat)<<std::endl;
                vertexData.push_back({tf * gv, glm::normalize(normalmat * gn), gc, gu});
                if(n.x != .0 || n.y != .0 || n.z != .0) recalc_normals = false;
            }

            if(recalc_normals)
            {
                int s = vertexData.size()-1;
                glm::vec3 v1 = vertexData[s-1].pos - vertexData[s].pos;
                glm::vec3 v2 = vertexData[s-2].pos - vertexData[s].pos;
                glm::vec3 n = glm::normalize(glm::cross(v1, v2));
                for(int i = 0; i < face->mNumIndices; i++)
                {
                    vertexData[s - i].norm = -n;
                }
            }
        }
    }

    buffer.upload(vertexData);

    idxData.resize(vertexData.size());

    for(int i = 0; i < vertexData.size(); i++)
    {
        idxData[i] = i;
    }

    objShader.attach("cube.vert");
    objShader.attach("manualuv.geom");
    objShader.attach("manualuv.frag");
    objShader.link();

    uvPosShader.attach("uv.vert");
    uvPosShader.attach("uv.geom");
    uvPosShader.attach("uv.frag");
    uvPosShader.link();

    color = {1.0,1.0,1.0};

    //glDisable(GL_CULL_FACE);

    buffer.attach(); CHECK_GL_ERROR;
    fb = nullptr;
};

ObjActor::~ObjActor() {

};


void ObjActor::render(SceneParams params) {

    swapReal();

    auto texLight = textureLightCurr->lockGL();

    objShader.activate(); CHECK_GL_ERROR;

    objShader.bind("view", params.view); CHECK_GL_ERROR;
    objShader.bind("projection", params.projection); CHECK_GL_ERROR;

    objShader.bind("inColor", color); CHECK_GL_ERROR;
    //objShader.bind("lightPos", params.lightPos); CHECK_GL_ERROR;
    //objShader.bind("viewPos", params.viewPos); CHECK_GL_ERROR;
    objShader.bind("u_pix", 1.0/texLight->getHeight()); CHECK_GL_ERROR;


    objShader.bind("tex", texLight); CHECK_GL_ERROR;

    if(textureColor != nullptr)
    {
        objShader.bind("texCol", textureColor); CHECK_GL_ERROR;
        objShader.bind("hasCol", 1); CHECK_GL_ERROR;
    }
    else
    {
        objShader.bind("hasCol", 0); CHECK_GL_ERROR;
    }

    buffer.bind();

    glDrawArrays(GL_TRIANGLES, 0, buffer.size()); CHECK_GL_ERROR;
};


RayMesh ObjActor::getMesh() {
    return {
        (float*) vertexData.data(),
        vertexData.size(),
        idxData.data(),
        idxData.size(),
        sizeof(ObjVertex)
    };
}


void ObjActor::renderUVPosNormTextures(
        int width,
        int height,
        std::shared_ptr<Texture> texturePos,
        std::shared_ptr<Texture> textureNorm
) {

    clock_t begin = clock();

    FrameBuffer fb(width, height);

    fb.addTexture(0, texturePos);
    fb.addTexture(1, textureNorm);

    fb.bind();

    GLenum DrawBuffers[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, DrawBuffers); // "1" is the size of DrawBuffers

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("wronk");
        return;
    }

    glViewport(0,0,width,height); CHECK_GL_ERROR;

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); CHECK_GL_ERROR;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); CHECK_GL_ERROR;

    uvPosShader.activate(); CHECK_GL_ERROR;
    uvPosShader.bind("u_half_pix", 1/1024.0); CHECK_GL_ERROR;

    buffer.bind();

    glDrawArrays(GL_TRIANGLES, 0, buffer.size()); CHECK_GL_ERROR;

    glFlush();
    glFinish();

    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    //printf(">>>>>>>>>>>>>t %f", elapsed_secs);

    fb.unbind();
}
