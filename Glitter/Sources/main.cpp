
#define _DEBUG
// Local Headers
#include "glitter.hpp"

// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#define GLFW_EXPOSE_NATIVE_X11
#define GLFW_EXPOSE_NATIVE_GLX
#include <GLFW/glfw3native.h>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <cube_actor.h>
#include <glm/gtc/matrix_transform.hpp>
#include <scene_params.h>
#include <obj_actor.h>
#include <assimp/cimport.h>
#include <iostream>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>

#include <radeon_rays.h>
#include <fsquad_actor.h>
#include <zconf.h>
#include <glm/vec4.hpp>
#include <ctime>
#include <CLWPlatform.h>
#include <CLWContext.h>
#include <CLWProgram.h>
#include <radeon_rays_cl.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <pthread.h>
#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <execinfo.h>
#include <signal.h>
#include <bits/types/stack_t.h>
#include <bits/types/siginfo_t.h>
#include <glm/gtx/string_cast.hpp>

void my_terminate(void);

namespace {
    // invoke set_terminate as part of global constant initialization
    static const std::terminate_handler SET_TERMINATE = std::set_terminate(my_terminate);
}

// This structure mirrors the one found in /usr/include/asm/ucontext.h
typedef struct _sig_ucontext {
    unsigned long     uc_flags;
    struct ucontext   *uc_link;
    stack_t           uc_stack;
    struct sigcontext uc_mcontext;
    sigset_t          uc_sigmask;
} sig_ucontext_t;

void crit_err_hdlr(int sig_num, siginfo_t * info, void * ucontext) {
    sig_ucontext_t * uc = (sig_ucontext_t *)ucontext;

    // Get the address at the time the signal was raised from the EIP (x86)
    void * caller_address = (void *) uc->uc_mcontext.rip;

    std::cerr << "signal " << sig_num
              << " (" << strsignal(sig_num) << "), address is "
              << info->si_addr << " from "
              << caller_address << std::endl;

    void * array[50];
    int size = backtrace(array, 50);

    std::cerr << __FUNCTION__ << " backtrace returned "
              << size << " frames\n\n";

    // overwrite sigaction with caller's address
    array[1] = caller_address;

    char ** messages = backtrace_symbols(array, size);

    // skip first stack frame (points here)
    for (int i = 1; i < size && messages != NULL; ++i) {
        std::cerr << "[bt]: (" << i << ") " << messages[i] << std::endl;
    }
    std::cerr << std::endl;

    free(messages);

    exit(EXIT_FAILURE);
}

void my_terminate() {
    static bool tried_throw = false;

    try {
        // try once to re-throw currently active exception
        if (!tried_throw++) throw;
    }
    catch (const std::exception &e) {
        std::cerr << __FUNCTION__ << " caught unhandled exception. what(): "
                  << e.what() << std::endl;
    }
    catch (...) {
        std::cerr << __FUNCTION__ << " caught unknown/unhandled exception."
                  << std::endl;
    }

    void * array[50];
    int size = backtrace(array, 50);

    std::cerr << __FUNCTION__ << " backtrace returned "
              << size << " frames\n\n";

    char ** messages = backtrace_symbols(array, size);

    for (int i = 0; i < size && messages != NULL; ++i) {
        std::cerr << "[bt]: (" << i << ") " << messages[i] << std::endl;
    }
    std::cerr << std::endl;

    free(messages);

    abort();
}
using namespace RadeonRays;

bool lbutton_down = false;

float3 ConvertFromBarycentric(const float* vec, const int* ind, int prim_id, const float4& uvwt)
{
    float3 a = { vec[ind[prim_id * 3] * 3],
                 vec[ind[prim_id * 3] * 3 + 1],
                 vec[ind[prim_id * 3] * 3 + 2], };

    float3 b = { vec[ind[prim_id * 3 + 1] * 3],
                 vec[ind[prim_id * 3 + 1] * 3 + 1],
                 vec[ind[prim_id * 3 + 1] * 3 + 2], };

    float3 c = { vec[ind[prim_id * 3 + 2] * 3],
                 vec[ind[prim_id * 3 + 2] * 3 + 1],
                 vec[ind[prim_id * 3 + 2] * 3 + 2], };
    return a * (1 - uvwt.x - uvwt.y) + b * uvwt.x + c * uvwt.y;
}

static void mouse_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        if(GLFW_PRESS == action)
            lbutton_down = true;
        else if(GLFW_RELEASE == action)
            lbutton_down = false;
    }
}

int main(int argc, char * argv[]) {

    // Load GLFW and Create a Window
    // --------------------------------------------------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_SAMPLES, 4);
    auto mWindow = glfwCreateWindow(mWidth, mHeight, "OpenGL", nullptr, nullptr);

    // Check for Valid Context
    if (mWindow == nullptr) {
        fprintf(stderr, "Failed to Create OpenGL Context");
        return EXIT_FAILURE;
    }
    glfwMakeContextCurrent(mWindow);
    gladLoadGL();
    // Create Context and Load OpenGL Functions
    fprintf(stderr, "OpenGL %s\n", glGetString(GL_VERSION));
    // --------------------------------------------------------------------------


    // --------------------------------------------------------------------------
    struct aiLogStream stream;
    stream = aiGetPredefinedLogStream(aiDefaultLogStream_STDOUT,NULL);
    aiAttachLogStream(&stream);
    // --------------------------------------------------------------------------


    Raytracer rt(2048, 2048, mWindow);



    /*
    // Model matrix : an identity matrix (model will be at the origin)
    glm::mat4 Model = glm::mat4(1.0f);
    // Our ModelViewProjection : multiplication of our 3 matrices
    glm::mat4 mvp = Projection * View * Model; // Remember, matrix multiplication is the other way around
     */

    // --------------------------------------------------------------------------
    SceneParams params;
    /*params.view = glm::lookAt(
            params.viewPos, // Camera is at (4,3,3), in World Space
            glm::vec3(0,10,0), // and looks at the origin
            glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
    );*/

    params.projection = glm::perspective(glm::radians(45.0f), (float) mWidth/ (float)mHeight  , 0.1f, 100.0f);

    params.lightPos = glm::vec3(0,0,200);
    // --------------------------------------------------------------------------




    //CubeActor* cube = new CubeActor(1);

    glm::mat4 tf = glm::scale(glm::mat4(1), {0.1, 0.1, 0.1});
    tf = glm::translate(tf, {0, 0, 0});
    //std::shared_ptr<ObjActor> traj = std::make_shared<ObjActor>(0, PROJECT_SOURCE_DIR "/Assets/Trajectories_sc_uv.obj", tf);
    std::shared_ptr<ObjActor> traj = std::make_shared<ObjActor>(0, PROJECT_SOURCE_DIR "/Assets/Trajectories_test_end6.obj", tf);

    glm::mat4 tf2 = glm::scale(glm::mat4(1), {0.1, -0.1, -0.1});
    tf2 = glm::translate(tf2, {0, -114.3, 0});
    std::shared_ptr<ObjActor> basemap = std::make_shared<ObjActor>(1, PROJECT_SOURCE_DIR "/Assets/Basemap_sc_bn.obj", tf2);

    glm::mat4 tf3 = glm::scale(glm::mat4(1), {0.1, 0.1, 0.1});
    tf3 = glm::translate(tf3, {0, 0, 0});
    std::shared_ptr<ObjActor> volu = std::make_shared<ObjActor>(2, PROJECT_SOURCE_DIR "/Assets/DMC_smooth.obj", tf3);

    glm::mat4 tf4 = glm::scale(glm::mat4(1), {0.1, 0.1, 0.1});
    tf3 = glm::translate(tf4, {0, 0, 0});
    std::shared_ptr<ObjActor> vsec = std::make_shared<ObjActor>(3, PROJECT_SOURCE_DIR "/Assets/vsec_sc_op.obj", tf4);


    int i = 0;


    glm::ivec2 t = {2048,2048};


    std::shared_ptr<Texture> texC;
    int width, height, bpp;
    unsigned char* rgb = stbi_load( PROJECT_SOURCE_DIR "/Assets/Basemap_sc_sm.obj.1.png", &width, &height, &bpp, 4 );

    if(rgb != NULL)
    {
        texC = std::make_shared<Texture>(width, height);
        texC->upload(rgb);
    }
    stbi_image_free( rgb );
    basemap->textureColor = texC;


    std::shared_ptr<Texture> texCV;
    rgb = stbi_load( PROJECT_SOURCE_DIR "/Assets/vsec_sc_op.obj.1.png", &width, &height, &bpp, 4 );

    if(rgb != NULL)
    {
        texCV = std::make_shared<Texture>(width, height);
        texCV->upload(rgb);
    }
    else{
        std::cout << "No Image?" << std::endl;
        exit(0);
    }
    stbi_image_free( rgb );
    vsec->textureColor = texCV;


    volu->color = {0.01,0.8,0.1};

    FSQuadActor quad(4);

    Texture tex(t.x, t.y);

    //quad.setTexture(volu->textureLightCurr);


    rt.addTracable(traj, true, true);
    rt.addTracable(volu, true, true);
    rt.addTracable(basemap, true, true);
    rt.addTracable(vsec, true, true);

    rt.start();

    int total_it = 100;

    std::srand(std::time(nullptr));

    /*for(int iteration = 0; iteration < total_it; iteration++)
    {

    }*/

    glfwSetMouseButtonCallback(mWindow, mouse_callback);

    bool prev_btn = false;

    glm::vec2 start;
    glm::mat4 viewStart;


    int iteration = 0;

    float fpsLimit = 30;

    float minFrameDur = 1.0 / fpsLimit;

    bool init = false;

    // Rendering Loop
    while (glfwWindowShouldClose(mWindow) == false) {
        clock_t startTime = clock();
        //std::unique_lock<std::mutex> lock(rt.renderMtx);
        //glfwMakeContextCurrent(mWindow);
        if (glfwGetKey(mWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(mWindow, true);

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // Background Fill Color
        glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glViewport(0,0,mWidth,mHeight);

        double xpos, ypos;
        glfwGetCursorPos(mWindow, &xpos, &ypos);

        if(prev_btn && !lbutton_down)
        {
            std::cout << "Cursor Position at (" << xpos << " : " << ypos << std::endl;
        }
        else if(!prev_btn && lbutton_down)
        {
            std::cout << "Cursor Position at (" << xpos << " : " << ypos << std::endl;
        }


        glm::vec3 rotv(0,0,0);
        bool srot = false;
        if (glfwGetKey(mWindow, GLFW_KEY_Q) == GLFW_PRESS) {
            rotv.z = -0.02;
            srot = true;
        }
        if (glfwGetKey(mWindow, GLFW_KEY_E) == GLFW_PRESS) {
            rotv.z = 0.02;
            srot = true;
        }

        if(lbutton_down)
        {
            rotv.x = (xpos-start.x)/1000;
            rotv.y = (ypos-start.y)/1000;
            srot = true;
        }
        if(srot)
        {
            glm::mat4 rot = glm::eulerAngleYXZ (rotv.x, rotv.y, rotv.z);
            params.view = rot * viewStart ;
        }
        prev_btn = lbutton_down;

        float stepsize = 0.1;

        glm::mat4 invView = glm::inverse(params.view);

        if (glfwGetKey(mWindow, GLFW_KEY_W) == GLFW_PRESS)
            params.view  = glm::translate(params.view, glm::vec3(invView * glm::vec4(0,0,stepsize,0)));
        if (glfwGetKey(mWindow, GLFW_KEY_S) == GLFW_PRESS)
            params.view  = glm::translate(params.view, glm::vec3(invView * glm::vec4(0,0,-stepsize,0)));
        if (glfwGetKey(mWindow, GLFW_KEY_A) == GLFW_PRESS)
            params.view  = glm::translate(params.view, glm::vec3(invView * glm::vec4(stepsize,0,0,0)));
        if (glfwGetKey(mWindow, GLFW_KEY_D) == GLFW_PRESS)
            params.view  = glm::translate(params.view, glm::vec3(invView * glm::vec4(-stepsize,0,0,0)));
        if (glfwGetKey(mWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
            params.view  = glm::translate(params.view, glm::vec3(invView * glm::vec4(0,-stepsize,0,0)));
        if (glfwGetKey(mWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
            params.view  = glm::translate(params.view, glm::vec3(invView * glm::vec4(0,stepsize,0,0)));

        params.viewPos = invView * glm::vec4(0,0,0,1);

        start = {xpos,ypos};
        viewStart = params.view;

        if(!init || glfwGetKey(mWindow, GLFW_KEY_1) == GLFW_PRESS)
        {
            // Cam 1
            params.viewPos = glm::vec3(-5.443983, -1.494369, 5.258400);
            params.view = {
                    {0.945314,  0.143127,  -0.293086, 0.000000},
                    {-0.322281, 0.548150,  -0.771803, 0.000000},
                    {0.050188,  0.824046,  0.564300,  0.000000},
                    {4.400755,  -2.734842, -5.716230, 1.000000}
            };
            init = true;
        }
        else if(glfwGetKey(mWindow, GLFW_KEY_2) == GLFW_PRESS)
        {
            // Cam 2
            params.viewPos = glm::vec3(-3.464425, 2.447311, 2.533818);
            params.view = {
                    {0.812920, 0.310083, -0.492980, 0.000000},
                    {-0.580930, 0.371880, -0.724053, 0.000000},
                    {-0.041187, 0.874972, 0.482443, 0.000000},
                    {4.342378, -2.052866, -1.158333, 1.000000}
            };
        }
        else if(glfwGetKey(mWindow, GLFW_KEY_3) == GLFW_PRESS)
        {
            // Cam 3
            params.viewPos = glm::vec3(2.324028, 4.615849, 0.897593);
            params.view = {
                    {-0.020092, -0.038246, 0.999074, 0.000000},
                    {0.999805, 0.001761, 0.020181, 0.000000},
                    {-0.002534, 0.999274, 0.038201, 0.000000},
                    {-4.565978, -0.816187, -2.449316, 1.000000}
            };
        }
        else if(glfwGetKey(mWindow, GLFW_KEY_4) == GLFW_PRESS)
        {
            // Cam 4
            params.viewPos = glm::vec3(-2.925833, 3.676794, 0.602557);
            params.view = {
                    {0.718384, 0.193691, -0.668159, 0.000000},
                    {-0.695079, 0.239171, -0.678009, 0.000000},
                    {0.028479, 0.951477, 0.306446, 0.000000},
                    {4.640371, -0.885993, 0.353326, 1.000000}
            };
        }
        else if(glfwGetKey(mWindow, GLFW_KEY_5) == GLFW_PRESS)
        {
            // Cam 5
            params.viewPos = glm::vec3(-6.185305, 5.256043, 2.585298);
            params.view = {
                    {0.035734, 0.435815, -0.899344, 0.000000},
                    {-0.999366, 0.011118, -0.034331, 0.000000},
                    {-0.004959, 0.899984, 0.435931, 0.000000},
                    {5.486562, 0.310480, -6.509284, 1.000000}
            };
        }

        std::cout<< "mtx" << glm::to_string(params.view)<<std::endl;
        std::cout<< "pos" << glm::to_string(params.viewPos)<<std::endl;


        traj->render(params);

        basemap->render(params);

        volu->render(params);

        vsec->render(params);

        //printf("painted\n");

        //quad.render(params);
        //quadPos.render(params);
        //basemap.render(params);
        // Flip Buffers and Draw
        glfwSwapBuffers(mWindow);
        glfwPollEvents();
        iteration ++;


        //glfwMakeContextCurrent(0);
        //lock.unlock();

        rt.tickMainThread();

        clock_t end = clock();
        double elapsed_secs = double(end - startTime) / CLOCKS_PER_SEC;

        double frameDst = minFrameDur - elapsed_secs;
        if(frameDst > 0)
        {
            //printf("frame_time: %f, sleep for %f", elapsed_secs, frameDst);
            std::this_thread::sleep_for(std::chrono::milliseconds((int)(1000.0*frameDst)));
        }

    }   glfwTerminate();

    return EXIT_SUCCESS;
}
