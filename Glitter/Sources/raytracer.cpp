//
// Created by ligx on 7/17/18.
//

#include <CLWPlatform.h>
#include <CLWContext.h>
#include <memory>
#include "raytracer.h"
#include "../Vendor/radeonrays/Calc/src/except_clw.h"
#include <radeon_rays_cl.h>
#include <ctime>

float valueOf(int x) {
    if (x == 0) return 0;
    if (x == 1) return 1;
    if (x == 2) return 0.5;
    if (x % 2 == 1) return 0.5 + valueOf(x + 1);
    else return 0.5*valueOf(x/2);
}

float angleOf(int x) {
    float res = 0.0;
    int depth = log2(x==0?1:x)/2;
    int p = x;
    while(depth >= 0)
    {
        int nextstart = pow(4, depth+1);
        int q = nextstart / 4;
        float fac = depth==0?1:pow(0.5, depth);
        if(p >= q && p < q*3) res += 45.0 * fac;
        p = p % q;
        depth--;
    }
    return res;
}

glm::vec2 displacement(int iter)
{
    if(iter == 0) return glm::vec2(0.0,0.0);
    else iter -= 1;
    int angle_base = 90 * ((iter/2)%2) + 180 * (iter%2);

    int depth = log2(iter==0?1:iter)/2;

    int start = depth==0 ? 0 : pow(4, depth);

    int nextstart = pow(4, depth+1);

    //int bigphase =

    int number_of_rings = pow(2, depth);

    int phase = iter/4 - start/4;

    int phasethirds = 1;

    int interphase = 0;

    int interinterphase = 0;

    float angle = angle_base + angleOf(iter/4);

    float r = 1.0;
    if(depth > 0)
    {
        phasethirds = (nextstart - start)/12;

        interphase = phase % phasethirds;

        interinterphase = interphase % (number_of_rings/2);

        if(phase < phasethirds)
        {
            r = valueOf(interinterphase+1+pow(2, depth-1));
        }
        else if(phase < phasethirds*2)
        {
            r = valueOf(interinterphase+1);
        }
        else
        {
            r = valueOf(interinterphase+1+pow(2, depth-1));
        }

    }

    return glm::vec2(cos(angle/180.0*M_PI)*(r),sin(angle/180.0*M_PI)*(r));
};

std::string getReason(cl_int errorcode)
{
    switch (errorcode)
    {
        case CL_INVALID_PROGRAM_EXECUTABLE:
            return "CL_INVALID_PROGRAM_EXECUTABLE";
        case CL_INVALID_COMMAND_QUEUE:
            return "CL_INVALID_COMMAND_QUEUE";
        case CL_INVALID_KERNEL:
            return "CL_INVALID_KERNEL";
        case CL_INVALID_CONTEXT:
            return "CL_INVALID_CONTEXT";
        case CL_INVALID_KERNEL_ARGS:
            return "CL_INVALID_KERNEL_ARGS";
        case CL_INVALID_WORK_DIMENSION:
            return "CL_INVALID_WORK_DIMENSION";
        case CL_INVALID_WORK_GROUP_SIZE:
            return "CL_INVALID_WORK_GROUP_SIZE";
        case CL_INVALID_WORK_ITEM_SIZE:
            return "CL_INVALID_WORK_ITEM_SIZE";
        case CL_INVALID_GLOBAL_OFFSET:
            return "CL_INVALID_GLOBAL_OFFSET";
        case CL_OUT_OF_RESOURCES:
            return "CL_OUT_OF_RESOURCES";
        case CL_MEM_OBJECT_ALLOCATION_FAILURE:
            return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
        case CL_INVALID_EVENT_WAIT_LIST:
            return "CL_INVALID_EVENT_WAIT_LIST";
        case CL_OUT_OF_HOST_MEMORY:
            return "CL_OUT_OF_HOST_MEMORY";
        case CL_SUCCESS:
            return "CL_SUCCESS";
        case CL_INVALID_VALUE:
            return "CL_INVALID_VALUE";
        case CL_INVALID_MIP_LEVEL:
            return "CL_INVALID_MIP_LEVEL";
        case CL_INVALID_GL_OBJECT:
            return "CL_INVALID_GL_OBJECT";
        case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
            return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
        case CL_INVALID_OPERATION:
            return "CL_INVALID_OPERATION";
        default:
            return "nothing like this: " + std::to_string(errorcode);
    }
}

Raytracer::Raytracer(int maxwidth, int maxheight, GLFWwindow* mWindowParent) :
        runthread(),
        stopThread(false),
        shapeIDNext(0),
        rayTexMaxSize(maxwidth, maxheight),
        renderedPosNorm(true)
{

    /*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
    mWindow2 = glfwCreateWindow(1, 1, "OpenGL", nullptr, mWindowParent);*/
    mWindow = mWindowParent;

    glfwMakeContextCurrent(mWindow);

    initCL(glfwGetGLXContext(mWindow), glfwGetX11Display());

    if(!ctx)
    {
        throw std::runtime_error("Could not initialize OpenCL context.");
    }

    initRR();

    if(!api)
    {
        throw std::runtime_error("Could not initialize RadeonRays.");
    }

    const char* kBuildopts(" -DKERNEL_PROGRAM -cl-mad-enable -cl-fast-relaxed-math -cl-std=CL1.2 -I " PROJECT_SOURCE_DIR "/Shaders/");
    try
    {
        clProgram = CLWProgram::CreateFromFile(PROJECT_SOURCE_DIR "/Shaders/kernel.cl", kBuildopts, ctx);
    }
    catch(CLWException e)
    {
        std::string reason = getReason(e.errcode_);
        throw std::runtime_error("Runtime Error while generating Rays: " + reason);
    }

    rayBuf = CLWBuffer<RadeonRays::ray>::Create(ctx, CL_MEM_READ_WRITE, rayTexMaxSize.x*rayTexMaxSize.y);
    rayBuffer = std::unique_ptr<RadeonRays::Buffer>(CreateFromOpenClBuffer(api.get(), rayBuf));

    occlBuf = CLWBuffer<int>::Create(ctx, CL_MEM_READ_WRITE, rayTexMaxSize.x*rayTexMaxSize.y);
    occlBuffer = std::unique_ptr<RadeonRays::Buffer>(CreateFromOpenClBuffer(api.get(), occlBuf));

    targetTex = std::make_shared<SharedImage>(rayTexMaxSize.x, rayTexMaxSize.y, GL_RGBA, GL_UNSIGNED_BYTE, SharedImage::MMODE_READ_WRITE);

    std::vector<uint8_t> imageData(rayTexMaxSize.x*rayTexMaxSize.y*4);
    std::fill(imageData.begin(), imageData.end(), 0);
    auto glTex = targetTex->lockGL();
    glTex->upload(imageData);

    textureNorm
            = std::make_shared<SharedImage>(rayTexMaxSize.x, rayTexMaxSize.y, GL_RGBA32F, GL_FLOAT, SharedImage::MMODE_READ_ONLY);
    texturePos
            = std::make_shared<SharedImage>(rayTexMaxSize.x, rayTexMaxSize.y, GL_RGBA32F, GL_FLOAT, SharedImage::MMODE_READ_ONLY);

    glfwMakeContextCurrent(mWindow);
}

void Raytracer::initCL(const GLXContext &glcCtx, const Display* x11Display) {

    std::vector<CLWPlatform> platforms;
    CLWPlatform::CreateAllPlatforms(platforms);

    if (platforms.size() == 0)
    {
        throw std::runtime_error("No OpenCL platforms installed.");
    }
    bool context_init;

    for (int i = 0; i < platforms.size(); ++i)
    {
        for (int d = 0; d < (int)platforms[i].GetDeviceCount(); ++d)
        {
            if (platforms[i].GetDevice(d).GetType() != CL_DEVICE_TYPE_GPU)
                continue;

            if (platforms[i].GetDevice(d).GetExtensions().find("cl_khr_gl_sharing") == std::string::npos)
                continue;

            std::cout << "name:" << platforms[i].GetDevice(d).GetName() << std::endl;

            cl_context_properties cps[] = {
                    CL_GL_CONTEXT_KHR, (cl_context_properties)glcCtx,
                    CL_GLX_DISPLAY_KHR, (cl_context_properties)x11Display,
                    CL_CONTEXT_PLATFORM, (cl_context_properties)(cl_platform_id)platforms[i],
                    0
            };

            ctx = CLWContext::Create(platforms[i].GetDevice(d), cps);

            break;
        }

        if (ctx)
            break;
    }
}

void Raytracer::initRR() {
    assert(ctx);

    cl_device_id id = ctx.GetDevice(0).GetID();
    cl_command_queue queue = ctx.GetCommandQueue(0);

    int nativeidx = -1;
    RadeonRays::IntersectionApi::SetPlatform(RadeonRays::DeviceInfo::kOpenCL);
    for (auto idx = 0U; idx < RadeonRays::IntersectionApi::GetDeviceCount(); ++idx)
    {
        RadeonRays::DeviceInfo devinfo;
        RadeonRays::IntersectionApi::GetDeviceInfo(idx, devinfo);

        if (devinfo.type == RadeonRays::DeviceInfo::kGpu && nativeidx == -1)
        {
            nativeidx = idx;
        }
    }
    if(nativeidx != -1)
    {
        api = std::shared_ptr<RadeonRays::IntersectionApi>(
                RadeonRays::CreateFromOpenClContext(ctx, id, queue),
                [] (RadeonRays::IntersectionApi* obj) {
                    RadeonRays::IntersectionApi::Delete(obj);
                }
        );
    }
}


Raytracer::~Raytracer() {
    stopThread = true;
    cv.notify_all();
    if(runthread.joinable()) runthread.join();

    api->DetachAll();

    for(RadeonRays::Shape* shape : shapes)
    {
        api->DeleteShape(shape);
    }
}

void Raytracer::addTracable(std::shared_ptr<Raytracable> tracable, bool source, bool target) {
    std::lock_guard<std::mutex> lock(mtxTracables);


    if(target)
    {
        tracable->initRT(rayTexMaxSize.x, rayTexMaxSize.y);
        tracables.push_back({tracable,0,0});
    }
    else{
        tracable->initRT(rayTexMaxSize.x, rayTexMaxSize.y, 255);
    }

    if(source)
    {
        RayMesh mesh = tracable->getMesh();
        RadeonRays::Shape *shape = api->CreateMesh(
                mesh.vertices,
                mesh.szVertices,
                mesh.byteStride,
                mesh.indices,
                0,
                nullptr,
                mesh.szIndices / 3
        );

        if(!shape)
            throw std::runtime_error("Could not create RadeonRays Shape");

        shape->SetId(shapeIDNext++);
        shapes.push_back(shape);

        api->AttachShape(shape);
    }
}

void Raytracer::start() {
    api->Commit();
    runthread = std::thread(&Raytracer::run, this);
}

void Raytracer::run()
{
    glfwMakeContextCurrent(0);

    try
    {
        while (!stopThread)
        {
            TraceableItem* canditate;
            if(tracables.size() > 0) canditate = &tracables[0];
            else continue;

            for (auto &tracable : tracables)
            {
                if(tracable.calcTime < canditate->calcTime) canditate = &tracable;
            }

            clock_t startTime = clock();

            {
                std::unique_lock<std::mutex> lock(renderMtx);
                renderedPosNorm = false;
                currTaceable = canditate->tracable;
                currTaceableIt = canditate->iteration;
                while(!renderedPosNorm)
                {
                    cv.wait(lock);
                }
                if(stopThread) break;
            }

            clock_t afterSyncTime = clock();

            //glfwMakeContextCurrent(mWindow);
            RayType type = canditate->iteration % 2 == 0 ? RT_DIRECT : RT_AMBIENT;

            createRaysCL(canditate->tracable, type, canditate->iteration);

            clock_t afterRayCTime = clock();

            RadeonRays::Event* event = nullptr;
            api->QueryOcclusion(rayBuffer.get(), rayTexMaxSize.x * rayTexMaxSize.y, occlBuffer.get(), nullptr,
                                &event);
            event->Wait();
            api->DeleteEvent(event);

            clock_t afterOcclTime = clock();

            calcIllumCL(canditate->tracable, type, canditate->iteration);

            clock_t afterIlluTime = clock();

            double elapsed_secs = double(afterIlluTime - startTime) / CLOCKS_PER_SEC;

            double elapsed_secs_sync = double(afterSyncTime - startTime) / CLOCKS_PER_SEC;
            double elapsed_secs_rayc = double(afterRayCTime - afterSyncTime) / CLOCKS_PER_SEC;
            double elapsed_secs_occl = double(afterOcclTime - afterRayCTime) / CLOCKS_PER_SEC;
            double elapsed_secs_illu = double(afterIlluTime - afterOcclTime) / CLOCKS_PER_SEC;

            printf("id:%i, pass:%s tot:%f, sync:%f, rayc:%f, occl:%f, illu:%f\n",
                   canditate->tracable->getID(),
                   canditate->iteration % 2 == 0 ? "direct" : "ambient",
                   elapsed_secs,
                   elapsed_secs_sync,
                   elapsed_secs_rayc,
                   elapsed_secs_occl,
                   elapsed_secs_illu
            );

            canditate->calcTime += elapsed_secs;
            canditate->iteration++;
        }
    }
    catch(std::runtime_error e){
        std::clog << "Runtime Error in CL Thread " << e.what() << std::endl;
        abort();
    }
    catch(CLWException e){
        std::string reason = getReason(e.errcode_);
        std::clog << "CLWException in CL Thread" << reason << std::endl;
        abort();
    }
    catch(Calc::ExceptionClw e){
        std::clog << "ExceptionClw in CL Thread " << e.what() << std::endl;
        abort();
    }
    catch(...)
    {
        std::exception_ptr p = std::current_exception();
        std::clog << "Unknown Error try calc rays: " << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
        abort();
    }

}

void Raytracer::createRaysCL(std::shared_ptr<Raytracable> tracable, RayType type, int iteration) {


    std::shared_ptr<SharedImage> normSI = textureNorm;//tracable->getNormTexture();
    std::shared_ptr<SharedImage> posSI = texturePos;//tracable->getPosTexture();

    std::shared_ptr<CLWImage2D> normClwi;
    std::shared_ptr<CLWImage2D> posClwi;

    try {
        normClwi = normSI->lockCL(ctx, SharedImage::MMODE_READ_ONLY);
        posClwi = posSI->lockCL(ctx, SharedImage::MMODE_READ_ONLY);
    }
    catch(CLWException e)
    {
        std::string reason = getReason(e.errcode_);
        std::clog << "Runtime Error try calc rays: " + reason << std::endl;
        exit(1);
    }
    catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << "Unknown Error try calc rays: " << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
        exit(1);
    }

    //run kernel
    CLWKernel kernel;
    if(type == RT_AMBIENT)
        kernel = clProgram.GetKernel("GenerateAORays");
    else if(type == RT_DIRECT)
        kernel = clProgram.GetKernel("GenerateShadowRays");

    glm::vec3 lightPos = { 0.0f, 0.0f, 200.0f};

    glm::vec2 lightPos_dist = displacement(iteration);

    glm::vec3 lightPosFin = lightPos + glm::vec3(lightPos_dist.x, lightPos_dist.y, 0.0f) * 0.0f;
    cl_float4 light = {lightPos.x,lightPos.y, lightPos.z, 0.0f};
    cl_uint2 randoms = {std::rand(), std::rand()};

    int arg = 0;
    kernel.SetArg(arg++, rayBuf);
    kernel.SetArg(arg++, *posClwi.get());
    kernel.SetArg(arg++, *normClwi.get());
    kernel.SetArg(arg++, rayTexMaxSize.x);
    kernel.SetArg(arg++, rayTexMaxSize.y);
    kernel.SetArg(arg++, light);
    kernel.SetArg(arg++, randoms);

    // Run generation kernel
    size_t gs[] = { static_cast<size_t>((rayTexMaxSize.x + 7) / 8 * 8), static_cast<size_t>((rayTexMaxSize.y + 7) / 8 * 8) };
    size_t ls[] = { 8, 8 };

    try
    {
        ctx.Launch2D(0, gs, ls, kernel);

        ctx.Flush(0);
        
        ctx.Finish(0);

    }
    catch(CLWException e)
    {
        std::string reason = getReason(e.errcode_);
        throw std::runtime_error("Runtime Error while generating Rays: " + reason);
    }
}

void Raytracer::calcIllumCL(std::shared_ptr<Raytracable> tracable, RayType type, int iteration) {

    std::shared_ptr<SharedImage> normSI = textureNorm;//tracable->getNormTexture();
    std::shared_ptr<SharedImage> posSI = texturePos;//tracable->getPosTexture();
    std::shared_ptr<SharedImage> sourceTexSI = tracable->getLastLightTexture();
    std::shared_ptr<SharedImage> targetTexSI = targetTex;

    std::shared_ptr<CLWImage2D> normClwi;
    std::shared_ptr<CLWImage2D> posClwi;
    std::shared_ptr<CLWImage2D> sourceTexClwi;
    std::shared_ptr<CLWImage2D> targetTexClwi;

    try {
        normClwi = normSI->lockCL(ctx, SharedImage::MMODE_READ_ONLY);
        posClwi = posSI->lockCL(ctx, SharedImage::MMODE_READ_ONLY);
        sourceTexClwi = sourceTexSI->lockCL(ctx, SharedImage::MMODE_READ_ONLY);
        targetTexClwi = targetTexSI->lockCL(ctx, SharedImage::MMODE_WRITE_ONLY);
    }
    catch(CLWException e)
    {
        std::string reason = getReason(e.errcode_);
        std::clog << "Runtime Error try calc illum: " + reason << std::endl;
        exit(1);
    }
    catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << "Unknown Error try calc illum: " << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
        exit(1);
    }

    //run kernel
    CLWKernel kernel;
    if(type == RT_AMBIENT)
        kernel = clProgram.GetKernel("ShadingAO");
    else if(type == RT_DIRECT)
        kernel = clProgram.GetKernel("ShadingDirect");

    int arg = 0;
    kernel.SetArg(arg++, rayBuf);
    //kernel.SetArg(arg++, isect_buf);
    kernel.SetArg(arg++, occlBuf);
    kernel.SetArg(arg++, *posClwi.get());
    kernel.SetArg(arg++, *normClwi.get());
    kernel.SetArg(arg++, *sourceTexClwi.get());
    kernel.SetArg(arg++, *targetTexClwi.get());
    kernel.SetArg(arg++, iteration);
    kernel.SetArg(arg++, rayTexMaxSize.x);
    kernel.SetArg(arg++, rayTexMaxSize.y);

    // Run generation kernel
    size_t gs[] = { static_cast<size_t>((rayTexMaxSize.x + 7) / 8 * 8), static_cast<size_t>((rayTexMaxSize.y + 7) / 8 * 8) };
    size_t ls[] = { 8, 8 };

    try
    {
        ctx.Launch2D(0, gs, ls, kernel);

        ctx.Flush(0);

        ctx.Finish(0);

        sourceTexClwi.reset();
        targetTexClwi.reset();

        tracable->swapLightTexture(targetTex);

    }
    catch(CLWException e)
    {
        std::string reason = getReason(e.errcode_);
        throw std::runtime_error("Runtime Error while generating Rays: " + reason);
    }
}

void Raytracer::tickMainThread() {
    std::unique_lock<std::mutex> lock(renderMtx);
    if(currTaceable && !renderedPosNorm)
    {
        currTaceable->renderUVPosNormTextures(rayTexMaxSize.x, rayTexMaxSize.y, texturePos->lockGL(), textureNorm->lockGL());

        //currTaceable->getLastLightTexture()->lockGL()->writeToFile("f_"+std::to_string(currTaceable->getID())+"_l.hdr");
        /*if(currTaceableIt == 0)
        {
            texturePos->lockGL()->writeToFile("f_"+std::to_string(currTaceable->getID())+"_p.hdr");
            textureNorm->lockGL()->writeToFile("f_"+std::to_string(currTaceable->getID())+"_n.hdr");
        }*/

        renderedPosNorm = true;
        cv.notify_all();
    }
}


