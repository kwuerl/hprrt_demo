//
// Created by ligx on 5/28/18.
//


#import "cube_actor.h"
#import "debug.h"

CubeActor::CubeActor(int id) : BaseRTActor(id) {
    /*vertexData.push_back({{-1.0,-1.0,0.0}});
    vertexData.push_back({{0.0,-1.0,0.0}});
    vertexData.push_back({{0.0,1.0,0.0}});*/
    //vertexData.push_back({{0.0,0.0,0.0},{0.0,0.0}});
    vertexData.push_back({{-1.0f,-1.0f,-1.0f}, {-1.0f, 0.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f,-1.0f, 1.0f}, {-1.0f, 0.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f, 1.0f, 1.0f}, {-1.0f, 0.0f, 0.0f}, {0.0,0.0}});

    vertexData.push_back({{ 1.0f, 1.0f,-1.0f}, { 0.0f, 0.0f,-1.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f,-1.0f,-1.0f}, { 0.0f, 0.0f,-1.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f, 1.0f,-1.0f}, { 0.0f, 0.0f,-1.0f}, {0.0,0.0}});

    vertexData.push_back({{ 1.0f,-1.0f, 1.0f}, { 0.0f,-1.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f,-1.0f,-1.0f}, { 0.0f,-1.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f,-1.0f,-1.0f}, { 0.0f,-1.0f, 0.0f}, {0.0,0.0}});

    vertexData.push_back({{ 1.0f, 1.0f,-1.0f}, { 0.0f, 0.0f,-1.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f,-1.0f,-1.0f}, { 0.0f, 0.0f,-1.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f,-1.0f,-1.0f}, { 0.0f, 0.0f,-1.0f}, {0.0,0.0}});

    vertexData.push_back({{-1.0f,-1.0f,-1.0f}, {-1.0f, 0.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f, 1.0f, 1.0f}, {-1.0f, 0.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f, 1.0f,-1.0f}, {-1.0f, 0.0f, 0.0f}, {0.0,0.0}});

    vertexData.push_back({{ 1.0f,-1.0f, 1.0f}, { 0.0f,-1.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f,-1.0f, 1.0f}, { 0.0f,-1.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f,-1.0f,-1.0f}, { 0.0f,-1.0f, 0.0f}, {0.0,0.0}});

    vertexData.push_back({{-1.0f, 1.0f, 1.0f}, { 0.0f, 0.0f, 1.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f,-1.0f, 1.0f}, { 0.0f, 0.0f, 1.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f,-1.0f, 1.0f}, { 0.0f, 0.0f, 1.0f}, {0.0,0.0}});

    vertexData.push_back({{ 1.0f, 1.0f, 1.0f}, { 1.0f, 0.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f,-1.0f,-1.0f}, { 1.0f, 0.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f, 1.0f,-1.0f}, { 1.0f, 0.0f, 0.0f}, {0.0,0.0}});

    vertexData.push_back({{ 1.0f,-1.0f,-1.0f}, { 1.0f, 0.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f, 1.0f, 1.0f}, { 1.0f, 0.0f, 0.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f,-1.0f, 1.0f}, { 1.0f, 0.0f, 0.0f}, {0.0,0.0}});

    vertexData.push_back({{ 1.0f, 1.0f, 1.0f}, { 0.0f, 1.0f,0.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f, 1.0f,-1.0f}, { 0.0f, 1.0f,0.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f, 1.0f,-1.0f}, { 0.0f, 1.0f,0.0f}, {0.0,0.0}});

    vertexData.push_back({{ 1.0f, 1.0f, 1.0f}, { 0.0f, 1.0f,0.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f, 1.0f,-1.0f}, { 0.0f, 1.0f,0.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f, 1.0f, 1.0f}, { 0.0f, 1.0f,0.0f}, {0.0,0.0}});

    vertexData.push_back({{ 1.0f, 1.0f, 1.0f}, { 0.0f, 0.0f, 1.0f}, {0.0,0.0}});
    vertexData.push_back({{-1.0f, 1.0f, 1.0f}, { 0.0f, 0.0f, 1.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f,-1.0f, 1.0f}, { 0.0f, 0.0f, 1.0f}, {0.0,0.0}});
    buffer.upload(vertexData);

    idxData.resize(vertexData.size());

    for(int i = 0; i < vertexData.size(); i++)
    {
        idxData[i] = i;
    }

    cubeShader.attach("cube.vert");
    cubeShader.attach("cube.frag");
    cubeShader.link();

    //glDisable(GL_CULL_FACE);

    buffer.attach(); CHECK_GL_ERROR;
};

CubeActor::~CubeActor() {

};

RayMesh CubeActor::getMesh() {
    return {
            (float*) vertexData.data(),
            vertexData.size(),
            idxData.data(),
            idxData.size(),
            sizeof(CubeVertex)
    };
}

void CubeActor::render(SceneParams params) {


    cubeShader.activate(); CHECK_GL_ERROR;

    cubeShader.bind("view", params.view); CHECK_GL_ERROR;
    cubeShader.bind("projection", params.projection); CHECK_GL_ERROR;

    cubeShader.bind("inColor", glm::vec3(1.0,1.0,1.0)); CHECK_GL_ERROR;
    cubeShader.bind("lightPos", params.lightPos); CHECK_GL_ERROR;
    cubeShader.bind("viewPos", params.viewPos); CHECK_GL_ERROR;

    buffer.bind();

    glDrawArrays(GL_TRIANGLES, 0, buffer.size()); CHECK_GL_ERROR;
};
