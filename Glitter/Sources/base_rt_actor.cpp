//
// Created by ligx on 7/8/18.
//
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>
#include "base_rt_actor.h"
#include <algorithm>
#include <iostream>
#include <radeon_rays_cl.h>
#include <CLWContext.h>
#include <CLWKernel.h>
#include <CL/cl_platform.h>
#include <cstdint>
#include <stdint-gcc.h>
#include <CL/cl.h>
#include <ctime>
#include <bits/types/clock_t.h>

BaseRTActor::BaseRTActor(int id) : Actor(id), Raytracable(id) {
    shouldSwap = false;
}

BaseRTActor::~BaseRTActor() {

}

void BaseRTActor::initRT(int width, int height, uint8_t fill) {
    /*textureNorm
            = std::make_shared<SharedImage>(width, height, GL_RGBA32F, GL_FLOAT, SharedImage::MMODE_READ_ONLY);
    texturePos
            = std::make_shared<SharedImage>(width, height, GL_RGBA32F, GL_FLOAT, SharedImage::MMODE_READ_ONLY);*/
    textureLightLast
            = std::make_shared<SharedImage>(width, height, GL_RGBA, GL_UNSIGNED_BYTE, SharedImage::MMODE_READ_WRITE);
    textureLightCurr
            = std::make_shared<SharedImage>(width, height, GL_RGBA, GL_UNSIGNED_BYTE, SharedImage::MMODE_READ_WRITE);

    std::vector<uint8_t> imageData(width*height*4);
    std::fill(imageData.begin(), imageData.end(), fill);
    auto glTex1 = textureLightLast->lockGL();
    glTex1->upload(imageData);
    std::fill(imageData.begin(), imageData.end(), fill);
    auto glTex2 = textureLightCurr->lockGL();
    glTex2->upload(imageData);

    //renderUVPosNormTextures(width, height, texturePos->lockGL(), textureNorm->lockGL());
}

/*std::shared_ptr<SharedImage> BaseRTActor::getPosTexture() {
    return texturePos;
}

std::shared_ptr<SharedImage> BaseRTActor::getNormTexture() {
    return textureNorm;
}*/

std::shared_ptr<SharedImage> BaseRTActor::getLastLightTexture() {
    return textureLightLast;
}

void BaseRTActor::swapLightTexture(std::shared_ptr<SharedImage>& newImage) {
    std::unique_lock<std::mutex> lock(mtx);
    textureLightLast.swap(newImage);
    shouldSwap = true;
}

void BaseRTActor::swapReal() {
    std::unique_lock<std::mutex> lock(mtx);
    if(shouldSwap)
    {
        auto texLast = textureLightLast->lockGL();
        auto texCurr = textureLightCurr->lockGL();
        glCopyImageSubData(
                texLast->getId(),    // src
                GL_TEXTURE_2D,       // src target
                0,                   // src mipmap
                0,                   // src x
                0,                   // src y
                0,                   // src z
                texCurr->getId(),    // dst
                GL_TEXTURE_2D,       // dst target
                0,
                0,                   // dst x
                0,                   // dst y
                0,                   // dst z
                texCurr->getWidth(), // dst width
                texCurr->getHeight(), // dst height
                1
        );
        shouldSwap = false;
    }
}



