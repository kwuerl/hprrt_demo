//
// Created by ligx on 30.06.18.
//

#include "fsquad_actor.h"
#import "debug.h"

FSQuadActor::FSQuadActor(int id) : Actor(id) {
    /*vertexData.push_back({{-1.0,-1.0,0.0}});
    vertexData.push_back({{0.0,-1.0,0.0}});
    vertexData.push_back({{0.0,1.0,0.0}});*/
    //vertexData.push_back({{0.0,0.0,0.0},{0.0,0.0}});
    vertexData.push_back({{-1.0f,-1.0f,0.0f}, {0.0f, 0.0f, 1.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f,-1.0f,0.0f}, {0.0f, 0.0f, 1.0f}, {1.0,0.0}});
    vertexData.push_back({{ 1.0f, 1.0f,0.0f}, {0.0f, 0.0f, 1.0f}, {1.0,1.0}});

    vertexData.push_back({{-1.0f,-1.0f,0.0f}, { 0.0f, 0.0f, 1.0f}, {0.0,0.0}});
    vertexData.push_back({{ 1.0f, 1.0f,0.0f}, { 0.0f, 0.0f, 1.0f}, {1.0,1.0}});
    vertexData.push_back({{-1.0f, 1.0f,0.0f}, { 0.0f, 0.0f, 1.0f}, {0.0,1.0}});

    buffer.upload(vertexData);


    quadShader.attach("fsq.vert");
    quadShader.attach("fsq.frag");
    quadShader.link();

    //glDisable(GL_CULL_FACE);

    buffer.attach(); CHECK_GL_ERROR;
};

FSQuadActor::~FSQuadActor() {
};

void FSQuadActor::render(SceneParams params) {
    quadShader.activate(); CHECK_GL_ERROR;
    quadShader.bind("tex", tex);
    buffer.bind();
    glDrawArrays(GL_TRIANGLES, 0, buffer.size()); CHECK_GL_ERROR;
};