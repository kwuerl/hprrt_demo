//
// Created by ligx on 7/2/18.
//

#include <glm/vec4.hpp>
#include <stb_image_write.h>
#include "texture.h"
#include "debug.h"

Texture::Texture(int width, int height, GLenum iformat, GLenum type) : width(width), height(height)
{
    glGenTextures(1, &textureId); CHECK_GL_ERROR;
    glBindTexture(GL_TEXTURE_2D, textureId); CHECK_GL_ERROR;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); CHECK_GL_ERROR;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); CHECK_GL_ERROR;
    glTexImage2D(GL_TEXTURE_2D, 0, iformat, width, height, 0, GL_RGBA, type, nullptr); CHECK_GL_ERROR;
    glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
}


Texture::~Texture()
{
    glDeleteTextures(1, &textureId); CHECK_GL_ERROR;
}


void Texture::upload(const std::vector<unsigned char> &tex_data)
{
    this->bind();
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, tex_data.data()); CHECK_GL_ERROR;
    this->unbind();
}


void Texture::upload(const unsigned char *tex_data)
{
    this->bind();
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, tex_data); CHECK_GL_ERROR;
    this->unbind();
}


void Texture::bind()
{
    glBindTexture(GL_TEXTURE_2D, textureId); CHECK_GL_ERROR;
}


void Texture::unbind()
{
    glBindTexture(GL_TEXTURE_2D, NULL); CHECK_GL_ERROR;
}

void Texture::read(std::vector<glm::vec4> &tex_data) {
    tex_data.resize(width*height);
    this->bind();
    glGetTexImage (GL_TEXTURE_2D,
                        0,
                        GL_RGBA, // GL will convert to this format
                        GL_FLOAT,   // Using this data type per-pixel
                        tex_data.data()); CHECK_GL_ERROR;
    this->unbind();

}


void Texture::writeToFile(std::string filename)
{
    std::vector<glm::vec4> tex_data;
    read(tex_data);
    stbi_write_hdr(filename.c_str(), width, height, 4, (float*)tex_data.data());
}

